FROM php:7.3-apache

#RUN apt-get update && \
#    apt-get install -y software-properties-common && \
#    rm -rf /var/lib/apt/lists/*
#
#RUN add-apt-repository ppa:openjdk-r/ppa

RUN apt-get update

RUN apt-get install -y \
    git \
    zip \
    wget \
    curl \
    sudo \
    unzip \    
    libicu-dev \
    libbz2-dev \
    libpng-dev \
    libjpeg-dev \    
    libmcrypt-dev \
    libreadline-dev \
    libfreetype6-dev \
    libzip-dev \
    g++ \
    vim \
    tzdata

# Install XDebug
RUN pecl install xdebug

# Configure XDebug
RUN docker-php-ext-enable xdebug

ENV APACHE_DOCUMENT_ROOT=/var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN a2enmod rewrite headers

RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

RUN docker-php-ext-install \
    bz2 \
    intl \
    iconv \
    bcmath \
    opcache \
    calendar \
    mbstring \
    pdo_mysql \
    gd \
    zip

RUN apt install -y libldap2-dev
RUN docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu
RUN docker-php-ext-install ldap


# Download and Install Sonar-Scanner
# RUN wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.5.0.2216.zip && \
#    unzip sonar-scanner-cli-4.5.0.2216.zip && \
#    rm sonar-scanner-cli-4.5.0.2216.zip && \
#    mv sonar-scanner-4.5.0.2216 /usr/lib/sonar-scanner && \
#    ln -s /usr/lib/sonar-scanner/bin/sonar-scanner /usr/local/bin/sonar-scanner

# Expose port 9000 and start php-fpm server
EXPOSE 9000

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

ARG uid

RUN echo $uid
RUN useradd -G www-data,root -u $uid devuser -m
RUN mkdir -p /home/devuser/.composer && \
    chown -R devuser:devuser /home/devuser
    
ENV TZ America/Sao_Paulo
