# Cadastro de pessoa-api

API para consumo do app cadastro de pessoa.

# Documentação

Para exeutar o projeto, você deverá ter instalado o docker em seu computador. <br />
Você poderá seguir esse passo a passo para exeutar a instalação: [clique aqui](https://balta.io/blog/docker-instalacao-configuracao-e-primeiros-passos) <br />

Após a instalação do docker, você deverá acessar a pasta do projeto e executar o seguinte comando: <br/>
`docker-compose up -d` <br/>

Você deverá realizar a configuração dos parâmetros necessários para rodar o projeto, que será: <br />
Realizar uma cópia do arquivo .env.example com o comando: <br />
`cp .env.example .env` <br/><br/>
As informações referentes à conexão com banco de dados deverá estar conforme abaixo: <br/>
`DB_CONNECTION=mysql`<br/>
`DB_HOST=172.19.0.100`<br/>
`DB_PORT=3306`<br/>
`DB_DATABASE=laravel`<br/>
`DB_USERNAME=usr_desenv`<br/>
`DB_PASSWORD=usr_desenv`<br/>

Você deverá executar o o comando `php artisan key:generate` <br/>

Após a configuração inicial do app com a subida dos containers docker, você deverá executar o comando abaixo para a criação das tabelas no banco de dados:<br />
`php artisan migrate` <br/>

Para visualizar o retorno da api acessando o seguinte endereço no browser: <br/>
`http://localhost:8000/api/v1/persons`