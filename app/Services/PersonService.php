<?php

namespace App\Services;

use App\Repositories\Contracts\PersonRepositoryInterface;
use App\Models\Person;

/**
 * Description of PersonService
 *
 * @author eder
 */
class PersonService
{

    protected $personRepository;

    function __construct(PersonRepositoryInterface $personRepository)
    {
        $this->personRepository = $personRepository;
    }

    private function formatCpf($cpf)
    {
        if ( strlen($cpf) > 0 ) {
            $newCpf = str_replace(".", "", $cpf);
            return str_replace("-", "", $newCpf);
        } else {
            return "";
        }
    }

    private function formatCnpj($cnpj)
    {
        if ( strlen($cnpj) > 0 ) {
            $newCnpj = str_replace(".", "", $cnpj);
            $new = str_replace("/", "", $newCnpj);
            return str_replace("-", "", $new);
        } else {
            return "";
        }
    }

    private function formatPhone($phone)
    {
        $newPhone = str_replace("(", "", $phone);
        $newPhone = str_replace(")", "", $newPhone);
        $newPhone = str_replace(" ", "", $newPhone);
        return str_replace("-", "", $newPhone);
    }

    public function makeNewPerson($request)
    {
        $personModel = new Person();

        $personModel->first_name = $request->first_name;
        $personModel->last_name = $request->last_name;
        $personModel->email = $request->email;
        $personModel->phone = $this->formatPhone($request->phone);
        $personModel->pessoa_fisica = $request->pessoa_fisica;
        $personModel->cpf = $this->formatCpf($request->cpf);
        $personModel->cnpj = $this->formatCnpj($request->cnpj);

        $contactRegistered = $this->personRepository->createNewPerson($personModel);

        return $contactRegistered;
    }

    public function getAllPersons()
    {
        $persons = $this->personRepository->getAllPersons();

        if ( count($persons) > 0 ) {
            return $persons;
        } else {
            return null;
        }
    }

    public function getOnePerson($id)
    {
        return $this->personRepository->getOnePerson($id);
    }

}
