<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequestCustom as FormRequest;

class PersonStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:100',
            'last_name' => 'required_if:pessoa_fisica,1|max:100',
            'email' => 'required|string|max:100|email:rfc,dns|regex:/^.+@.+$/i',
            'phone' => 'required|string|celular_com_ddd|max:15',
            'pessoa_fisica' => 'required',
            'cpf' => 'required_if:pessoa_fisica,1',
            'cnpj' => 'required_if:pessoa_fisica,0',
        ];
    }
}
