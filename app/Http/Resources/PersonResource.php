<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PersonResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'phone' => $this->formatPhoneResource($this->phone),
            'pessoa_fisica' => $this->pessoa_fisica,
            'cpf' => $this->formatCpfResource($this->cpf),
            'cnpj' => $this->formatCnpjResource($this->cnpj),
        ];
    }

    private function formatCpfResource($cpf)
    {
        if ( strlen($cpf) > 0 ) {
            return substr($cpf, 0, 3) . '.' .
                    substr($cpf, 3, 3) . '.' .
                    substr($cpf, 6, 3) . '-' .
                    substr($cpf, 9, 2);
        } else {
            return null;
        }
    }

    private function formatCnpjResource($cnpj)
    {
        if ( strlen($cnpj > 0) ) {
            return substr($cnpj, 0, 2) . '.' .
                    substr($cnpj, 2, 3) . '.' .
                    substr($cnpj, 5, 3) . '/' .
                    substr($cnpj, 8, 4) . '-' .
                    substr($cnpj, -2);
        } else {
            return null;
        }
    }

    private function formatPhoneResource($phone)
    {
        if ( strlen($phone > 0) ) {
            if ( strlen($phone) === 11 ) {
                return substr($phone, 0, 0) . '(' .
                        substr($phone, 0, 2) . ') ' .
                        substr($phone, 2, 5) . '-' .
                        substr($phone, 7, 8);
            } else {
                return substr($phone, 0, 0) . '(' .
                        substr($phone, 0, 2) . ') ' .
                        substr($phone, 2, 4) . '-' .
                        substr($phone, 6, 8);
            }
        } else {
            return null;
        }
    }

}
