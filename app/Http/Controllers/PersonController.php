<?php

namespace App\Http\Controllers;

use App\Http\Requests\PersonStoreRequest;
use App\Services\PersonService;
use App\Http\Resources\PersonResource;

class PersonController extends Controller
{
    
    protected $service;

    function __construct(PersonService $service)
    {
        $this->service = $service;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $persons = $this->service->getAllPersons();
            if ( $persons !== null ) {
                return response()->json([
                            'data' => PersonResource::collection($persons),
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Nenhuma pessoa cadastrada.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\PersonStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PersonStoreRequest $request)
    {
        try {
            $person = $this->service->makeNewPerson($request);
            if ( $person !== null ) {
                return response()->json([
                            'data' => new PersonResource($person),
                            'statusCode' => 201,
                                ], 201);
            } else {
                return response()->json([
                            'message' => 'Erro ao cadastrar a pessoa.',
                            'statusCode' => 422
                                ], 422);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $person = $this->service->getOnePerson($id);
        try {
            if ( $person !== null ) {
                return response()->json([
                            'data' => new PersonResource($person),
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Pessoa não encontrada.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\PersonStoreRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PersonStoreRequest $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
