<?php

namespace App\Repositories\Contracts;

/**
 *
 * @author eder
 */
use App\Models\Person;

interface PersonRepositoryInterface
{

    /**
     * Construtor inicial da classe
     * @param Person $person
     */
    public function __construct(Person $person);

    /**
     * Cria um nova pessoa no banco de dados
     * @param Person $person
     */
    public function createNewPerson(Person $person);

    /**
     * Recupera todos as pessoas do banco de dados
     */
    public function getAllPersons();
    
    /**
     * Recupera os dados de uma pessoa
     * @param int $id Id da pessoa a ser pesquisada 
     */
    public function getOnePerson($id);
    
}
