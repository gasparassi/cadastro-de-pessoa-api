<?php

namespace App\Repositories;

use App\Repositories\Contracts\PersonRepositoryInterface;
use App\Models\Person;

/**
 * Description of PersonRepository
 *
 * @author eder
 */
class PersonRepository implements PersonRepositoryInterface
{

    private $entity;

    /**
     * Construtor inicial da classe
     * @param Person $person
     */
    public function __construct(Person $person)
    {
        $this->entity = $person;
    }

    /**
     * Cria uma nova pessoa no banco de dados
     * @param Person $person
     */
    public function createNewPerson(Person $person)
    {
        return $this->entity->create($person->toArray());
    }

    /**
     * Recupera todas as pessoas do banco de dados
     */
    public function getAllPersons()
    {
        return $this->entity->all();
    }

    /**
     * Recupera as informações de uma pessoa
     * @param int $id
     */
    public function getOnePerson($id)
    {
        return $this->entity->find($id);
    }

}
