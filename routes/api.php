<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PersonController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/', function () {
    return response()
                    ->json([
                        'message' => 'Cadastro de Pessoas API',
                        'status' => 'Connected',
                        'statusCode' => 200,
                            ],
                            200);
}
);

Route::group([
//    'middleware' => 'users',
    'prefix' => 'v1',
        ], function () {
    Route::group([
        'prefix' => 'persons',
            ], function () {
        Route::post('/', [PersonController::class, 'store'])->name('pessoas.store');
        Route::get('/', [PersonController::class, 'index'])->name('pessoas.index');
        Route::get('/{id}', [PersonController::class, 'show'])->name('pessoas.show');
    });
});

